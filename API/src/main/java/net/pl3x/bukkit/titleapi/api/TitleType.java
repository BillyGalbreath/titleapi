package net.pl3x.bukkit.titleapi.api;

public enum TitleType {
    TITLE,
    SUBTITLE,
    TIMES,
    CLEAR,
    RESET
}
