package net.pl3x.bukkit.titleapi.api;

import org.bukkit.entity.Player;

public interface TitlePacket {
    void setText(String text);

    void setType(TitleType type);

    void setTimes(int fadeIn, int stay, int fadeOut);

    void send(Player player);

    void broadcast();
}
