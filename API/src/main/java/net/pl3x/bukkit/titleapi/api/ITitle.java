package net.pl3x.bukkit.titleapi.api;

import org.bukkit.entity.Player;

public interface ITitle {
    String getText();

    void setText(String text);

    TitleType getType();

    void setType(TitleType type);

    int[] getTimes();

    void setTimes(int fadeIn, int stay, int fadeOut);

    void send(Player player);

    void broadcast();
}
