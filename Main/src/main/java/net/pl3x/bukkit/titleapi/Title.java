package net.pl3x.bukkit.titleapi;

import net.pl3x.bukkit.titleapi.api.ITitle;
import net.pl3x.bukkit.titleapi.api.TitlePacket;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Title implements ITitle {
    private String text;
    private TitleType type;
    private int fadeIn;
    private int stay;
    private int fadeOut;

    public Title(String text) {
        this(TitleType.TITLE, text);
    }

    public Title(TitleType type, String text) {
        this(type, text, 20, 60, 20);
    }

    public Title(int faceIn, int stay, int fadeOut) {
        this(TitleType.TIMES, null, faceIn, stay, fadeOut);
    }

    public Title(TitleType type, String text, int fadeIn, int stay, int fadeOut) {
        this.text = text;
        this.type = type;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public TitleType getType() {
        return type;
    }

    @Override
    public void setType(TitleType type) {
        this.type = type;
    }

    @Override
    public int[] getTimes() {
        return new int[]{fadeIn, stay, fadeOut};
    }

    @Override
    public void setTimes(int fadeIn, int stay, int fadeOut) {
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    @Override
    public void send(Player player) {
        TitlePacket packet = getPacket();
        if (packet == null) {
            return;
        }
        packet.send(player);
    }

    @Override
    public void broadcast() {
        TitlePacket packet = getPacket();
        if (packet == null) {
            return;
        }
        Bukkit.getOnlinePlayers().forEach(packet::send);
    }

    private TitlePacket getPacket() {
        String packageName = Bukkit.getServer().getClass().getPackage().getName();
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);
        String path = this.getClass().getPackage().getName() + ".nms." + version;

        try {
            final Class<?> clazz = Class.forName(path + ".PacketHandler");
            if (TitlePacket.class.isAssignableFrom(clazz)) {
                return (TitlePacket) clazz.getConstructor(ITitle.class).newInstance(this);
            }
        } catch (Exception e) {
            Bukkit.getLogger().info("[ERROR] This plugin is not compatible with this server version (" + version + ").");
            Bukkit.getLogger().info("[ERROR] Could not send title packet!");
            e.printStackTrace();
        }
        return null;
    }
}
