package net.pl3x.bukkit.titleapi.nms.v1_8_R1;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.EnumTitleAction;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;
import net.pl3x.bukkit.titleapi.api.ITitle;
import net.pl3x.bukkit.titleapi.api.TitlePacket;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketHandler implements TitlePacket {
    private IChatBaseComponent component;
    private EnumTitleAction type;
    private int fadeIn = 20;
    private int stay = 60;
    private int fadeOut = 20;

    public PacketHandler(ITitle title) {
        setText(title.getText());
        setType(title.getType());
        int[] times = title.getTimes();
        setTimes(times[0], times[1], times[2]);
    }

    public void setText(String text) {
        if (text == null) {
            return;
        }
        this.component = ChatSerializer.a("{\"text\":\"" + ChatColor.translateAlternateColorCodes('&', text) + "\"}");
    }

    public void setType(TitleType type) {
        this.type = EnumTitleAction.a(type.name());
    }

    public void setTimes(int fadeIn, int stay, int fadeOut) {
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    @Override
    public void send(Player player) {
        if (player == null || !player.isOnline()) {
            return;
        }
        PacketPlayOutTitle packet = new PacketPlayOutTitle(type, component, fadeIn, stay, fadeOut);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    @Override
    public void broadcast() {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(type, component, fadeIn, stay, fadeOut);
        for (Player online : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
        }
    }
}
